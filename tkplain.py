#!/usr/bin/env python

from Tkinter import *
from tkprojector import Projector
import threading
from time import sleep

import control

def grid(col, row, thing, colspan=0, rowspan=0):
    thing.grid(column=col, row=row)
    if colspan:
        thing.grid(columnspan=colspan)
    if rowspan:
        thing.grid(rowspan=rowspan)
    return thing

def simplespin(parent, **kwargs):
    box = Spinbox(parent, **kwargs)
    #box.bind('<Return>', lambda e: callback(e.widget.get()))
    #box.bind('<FocusOut>', lambda e: callback(e.widget.get()))
    return box

def spin_cr_group(parent, col, row, label, variable, callback):
    frame = grid(col, row, LabelFrame(parent, text=label), colspan=2)

    grid(0,0, Label(frame, text="Cols:"))
    cols_spinner = grid(1, 0, simplespin(frame, textvariable=variable.cols))

    grid(2,0, Label(frame, text="Rows:"))
    rows_spinner = grid(3, 0, simplespin(frame, textvariable=variable.rows))

    grid(4,0, Button(frame, text="Do it!", command=callback))

    return cols_spinner, rows_spinner

def pprint(x):
    print x
    print dir(x)

class App(Frame):

    connected = False

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.grid(sticky=(N,S,E,W), padx=3)

        self.p = Projector()

        self.createNetworkWidgets()

        self.master.wm_title('Projector Control')
        self.master.minsize(500, 500)
        self.master.lift()

    def createNetworkWidgets(self):

        networkFrame =      grid(0, 0, LabelFrame(self, text='Network'), colspan=2)

        ######################################################################
        #
        # Network:

        grid(0,0,Label(networkFrame, text="IP:"))
        self.hostIP = grid(1,0, Entry(networkFrame))
        try:
            self.hostIP.insert(0, open('/tmp/.projector_ip').read())
        except IOError:
            pass

        self.hostIP.bind('<Return>', lambda e: self.connect_clicked())
        connectButton = grid(2,0, Button(networkFrame, text="Connect",
            command=self.connect_clicked))

        grid(3,0, Button(networkFrame, text='Quit', command=self.quit))


    def createWidgets(self):

        ######################################################################
        #
        # Main Boxes:


        powerFrame =        grid(0, 1, LabelFrame(self, text='Power'))
        testPatFrame =      grid(1, 1, LabelFrame(self, text='Test Patterns'))

        colourTempFrame =   grid(2, 0, LabelFrame(self, text='Colour Temperature'))
        brightnessFrame =   grid(3, 0, LabelFrame(self, text='Brightness'))
        contrastFrame   =   grid(3, 1, LabelFrame(self, text='Contrast'))

        # bottom row:

        lensControlFrame =  grid(0, 3, LabelFrame(self, text='Lens Control'), colspan=2)
        imageControlFrame = grid(2, 3, LabelFrame(self, text='Image Control'), colspan=2)



        #######################################################################
        #
        # Power:


        self.powerOn = grid(0,0, Button(powerFrame,
            text='Power ON',
            command=self.power_on))
        self.powerOff = grid(0,1, Button(powerFrame,
            text='Power OFF',
            command=self.power_off))

        self.powerLevel = grid(0,2, Scale(powerFrame,
            label='Power Level',
            orient='horizontal',
            variable=self.p.powerlevel,
            command=self.power_level))

        self.initstuff = grid(0,3, Button(powerFrame,
            text='Init Stuff...',
            command=self.initstuff
            ))


        #######################################################################
        #
        # Test Patterns:

        testPatternMenu = grid(0,0, OptionMenu(testPatFrame, self.p.testpattern,
            "none",
            "black","white","green","red","blue",
            "checker","align","h_ramp","v_ramp","max_lumens","native_white",
            command=self.select_testpattern))
        testPatternMenu = grid(0,1, Button(testPatFrame,
            command=self.remove_testpattern,
            text='Remove Test Pattern'))

        #######################################################################
        #
        # Colour Temp:


        def RGB(parent, command, variablegroup):
            r = grid(0,0, Scale(parent,
                            label='R',
                            variable=variablegroup.red,
                            command=command))
            g = grid(1,0, Scale(parent,
                            label='G',
                            variable=variablegroup.green,
                            command=command))
            b = grid(2,0, Scale(parent,
                            label='B',
                            variable=variablegroup.blue,
                            command=command))
            return r,g,b


        def RGBO(parent, command, variablegroup):
            r,g,b = RGB(parent, command, variablegroup)
            o = grid(3,0, Scale(parent,
                            label='Overall',
                            variable=variablegroup.overall,
                            command=command))

            #a = grid(4,0, Checkbox(parent,
            #                label='Auto',
            #                variable=variablegroup.auto,
            #                command=command))

            return r,g,b,o


        RGB(colourTempFrame, self.color_temp, self.p.color_temp)

        ######################################################################
        #
        # Brightness:

        RGBO(brightnessFrame, self.brightness, self.p.brightness)

        ######################################################################
        #
        # Contrast:


        RGBO(contrastFrame, self.contrast, self.p.contrast)


        #######################################################################
        #
        # Lens Control:


        lensShiftH = grid(0,0, Scale(lensControlFrame,
            label='Shift Horizontal',
            command=self.lens,
            variable=self.p.lensh,
            from_=0, to=3000,
            orient='horizontal'))

        lensShiftV = grid(0,1, Scale(lensControlFrame,
            label='Shift Vertical',
            command=self.lens,
            variable=self.p.lensv,
            from_=0, to=3000,
            orient='horizontal'))

        lensFocus = grid(0,2, Scale(lensControlFrame,
            label='Focus',
            command=self.lens,
            variable=self.p.lensf,
            from_=0, to=3000,
            orient='horizontal'))

        lensZoom = grid(0,3, Scale(lensControlFrame,
            label='Zoom',
            command=self.lens,
            variable=self.p.lensz,
            from_=0, to=3000,
            orient='horizontal'))

        shutterButton = grid(0,4, Button(lensControlFrame,
            text='STOP',
            command=self.STOP))


        #######################################################################
        #
        # Image Control:


        grid(0,0,Label(imageControlFrame, text='Orientation:'))

        self.orientOption = StringVar(self.master)
        self.orientOption.set('Front Projection')

        orient = grid(1,0, OptionMenu(imageControlFrame, self.orientOption,
            "Front Projection", "Rear Projection", "Front (Invert)", "Rear (Invert)",
            command=self.orientation_select))

        parked = grid(0,1, Checkbutton(imageControlFrame,
            command=self.img_parked,
            text='Mirror Parked'))

        inputSizeCols, inputSizeRows = spin_cr_group(imageControlFrame, 0, 2,
            "Input Size:",
            self.p.imagesize,
            self.img_input_size)

        inputWindowCols, inputWindowRows = spin_cr_group(imageControlFrame, 0, 3,
            "Input Window Size:",
            self.p.inputimagewindowsize,
            self.inputimagewindowsize)

        inputFrameCols, inputFrameRows = spin_cr_group(imageControlFrame, 0, 4,
            "Input Window Frame:",
            self.p.inputposition,
            self.inputposition)

        imgCaptureCols, imgCaptureRows = spin_cr_group(imageControlFrame, 0, 6,
            "Capture Position:",
            self.p.captureposition,
            self.captureposition)


# CALLBACKS:

    def power_on(self):
        # STUB
        print 'power ON'
        threading.Thread(target=self.connection.lamp_on).start()

    def power_off(self):
        # STUB
        print 'power OFF'
        threading.Thread(target=self.connection.lamp_off).start()

    def power_level(self, lev):
        # STUB
        print 'power_level:', lev
        self.connection.lamp_power(power=lev)

    ##################


    def select_testpattern(self, value):
        self.connection.testpattern(value)
        self.connection.projectionmode('testpattern')

    def remove_testpattern(self):
        self.p.testpattern.set('none')
        self.connection.testpattern('none')
        #self.connection.projectionmode('normal')

    ##################

    def color_temp(self, *vargs):
        self.connection.img_color_temp(
            self.p.color_temp.red.get(),
            self.p.color_temp.green.get(),
            self.p.color_temp.blue.get()
        )


    def brightness(self, *vargs):
        self.connection.img_brightness(
            self.p.brightness.red.get(),
            self.p.brightness.green.get(),
            self.p.brightness.blue.get(),
            self.p.brightness.overall.get(),
            self.p.brightness.auto.get()
        )

    def contrast(self, *vargs):
        self.connection.img_contrast(
            self.p.contrast.red.get(),
            self.p.contrast.green.get(),
            self.p.contrast.blue.get(),
            self.p.contrast.overall.get(),
            self.p.contrast.auto.get()
        )


    ###################

    def lens(self, *vargs):
        print 'lens_movement'
        self.connection.lens_goto(
            self.p.lensh.get(), self.p.lensv.get(), self.p.lensf.get(), self.p.lensz.get()
            )


    #####################

    def img_parked(self):
        # STUB
        print 'Parked clicked...'

    def img_input_size(self, *vargs):
        # STUB
        print 'Image Input Size Cols:'
        self.connection.simplecommand('imageposition', 'image,imageposition,write,c',
            self.p.inputposition.cols.get(), 'r', self.p.inputposition.rows.get())

    def inputimagewindowsize(self, *ev):
        # STUB
        print 'Image Input Window Cols:'
        self.connection.simplecommand('input window', 'image,imageinputwindow,write,c',
            self.p.imagesize.cols.get(), 'r', self.p.imagesize.rows.get())

    def img_input_window_frame(self, *lev):
        # STUB
        print 'Image Input Window Frame Cols:'
        self.connection.simplecommand('window2?', 'image,imageinputwindow,write,c',
            self.p.imagesize.cols.get(), 'r', self.p.imagesize.rows.get())

    def inputposition(self, *lev):
        # STUB
        print 'Image Position Frame Cols:'
        self.connection.simplecommand('image,imageinputwindow,write,c',
            self.p.imagesize.cols.get(), 'r', self.p.imagesize.rows.get())

    def captureposition(self, lev):
        # STUB
        print 'Capture Pos Cols:', lev

    def orientation_select(self, value):
        code = 'normal'

        if value == "Front Projection":
            code = 'normal'
        elif value == "Rear Projection":
            code = 'nsflip'
        elif value == "Front (Invert)":
            code = 'ewflip'
        elif value == "Rear (Invert)":
            code = 'newsflip'
        else:
            print 'Unknown Value for orientation!!!'

        self.connection.orient(code)
        print 'Setting Orientation to :', code

    def connect_clicked(self):
        addr = self.hostIP.get()
        try:
            with open('/tmp/.projector_ip', 'w') as f:
                f.write(addr)
        except IOError:
            pass

        if not self.connected:
            self.connection = control.Controller(addr, 7000)
            self.connection.connect()
            self.createWidgets()
            #self.connection.live = True
            def stupid_phantom_startup():
                sleep(5)
                self.connection.live = True

            threading.Thread(target=stupid_phantom_startup).start()

        else:
            if self.connection:
                self.connection.connect()

    def STOP(self, *vargs):
        self.connection.simplecommand('STOP LENS MOVEMENT!', 'lens,stop,write')

    def initstuff(self, *vargs):
        self.connection.initstuff()

    def quit(self):
        self.master.quit()




if __name__ == '__main__':
    root = Tk()
    app = App(master=root)

    root.title("Projector Control")

    root.call('wm', 'attributes','.','-topmost',True)
    root.after_idle(root.call, 'wm','attributes','.', '-topmost', False)
    app.mainloop()
    try:
        root.destroy()
    except:
        exit
