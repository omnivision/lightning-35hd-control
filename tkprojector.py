from Tkinter import StringVar, IntVar, BooleanVar, StringVar

def StringSet(value):
    ''' Wrapper for creating and setting in one go. '''
    x = StringVar()
    x.set(value)
    return x

class BrightnessContrast(object):
    def __init__(self):
        self.red = IntVar()
        self.green = IntVar()
        self.blue = IntVar()

        self.overall = IntVar()
        self.auto = StringSet('off')

class RGB(object):
    def __init__(self):
        self.red = IntVar()
        self.green = IntVar()
        self.blue = IntVar()

class CR(object):
    def __init__(self):
        self.cols = IntVar()
        self.rows = IntVar()

        self.cols.set(640)
        self.rows.set(480)

class Projector(object):
    def __init__(self):
        self.powerlevel = IntVar()
        self.brightness = BrightnessContrast()
        self.contrast = BrightnessContrast()
        self.projectionmode = StringSet('normal') # curtain,testpattrn,normal
        self.orientation = StringSet('normal') # normal,nsflip,ewflip,newsflip
        self.mirrorpark = StringSet('park') # park,release
        self.color_temp = RGB()
        self.imagesize = CR()
        self.inputimagewindowsize = CR()
        self.inputposition = CR()
        self.captureposition = CR()

        self.lensh = IntVar()
        self.lensh.set(2996)
        self.lensv = IntVar()
        self.lensv.set(1598)
        self.lensf = IntVar()
        self.lensf.set(4017)
        self.lensz = IntVar()
        self.lensz.set(1674)

        self.testpattern = StringSet('off')
        self.testrate = IntVar()
        self.testrate.set(60)
            # name of black,white,green,red,blue,
            #          checker,align,h_ramp,v_ramp,max_lumens,native_white

        # Degamma?

        self.picmute = StringSet('off')


