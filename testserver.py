import socket

HOST='127.0.0.1'
PORT=7000

MSGID=0
USER=1
TIME=2
PRIOR=3
PROJID=4
PROJNAME=5

while True:

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((HOST, PORT))
    s.listen(1)
    conn, addr = s.accept()
    print 'Connected by', addr
    while 1:
        data = conn.recv(1024)
        if not data: break
        print '<-', data
        parts = data.split(',')
        ack = parts[MSGID],'ACK', ','.join(parts[6:])
        to_return = ','.join(ack)
        print '->', to_return
        conn.sendall(to_return)
    conn.close()
