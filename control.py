import socket
import threading
from datetime import datetime
from time import sleep

class CommandFailed(Exception):
    pass

def clamp(down, up, value):
    return min(up, max(down, type(down)(value)))

################################################################################

# for controlling a projector:

class Controller(object):

    def __init__(self, hostname, port):
        self.host = hostname
        self.port = int(port)

        self.msgid = 1
        self.user = 'external'
        self.projectorid = 0
        self.projectorname = 'DPL-123'
        self.live = False

        self.queue = []
        self.queuelock = threading.Lock()

    def connect(self, timeout=8):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.host, self.port))
        self.socket.settimeout(timeout)

        self.threadloop = threading.Thread(target=self.eventloop)
        self.threadloop.daemon = True
        self.threadloop.start()

##########################

    def _make_msg(self, *vargs):
        return ','.join(str(m) for m in vargs)

    def eventloop(self):
        while True:
            self.queuelock.acquire()
            if len(self.queue):
                do_now = self.queue.pop(0)
                self.queuelock.release()
                self._send(*do_now)
                data = self._get_resp()
                print 'RECV:', data
            else:
                self.queuelock.release()
                sleep(0.4)

    def _send(self, msg, *vargs):

        msg = ','.join([msg] + [str(x) for x in vargs])

        # TODO:
        priority = 0
        self.msgid += 1

        now = datetime.now().strftime('%H:%M:%S,%d/%m/%Y')
        full_msg = '\n%s,%s,%s,%s,%s,%s,%s\r' % ( \
            self.msgid, self.user, now, priority,
            self.projectorid, self.projectorname,
            msg)

        print 'MSG:', full_msg

        self.socket.sendall(full_msg)

    def _get_resp(self):

        data = self.socket.recv(1024)

        print data

        rec = data.split(',')

        if len(rec) == 3:
            if rec[0] != self.msgid:
                print 'messages out of order!'
            else:
                if rec[1] == 'NAK':
                    print 'command failed!'
                return rec
        elif len(rec)>3:
            return rec[0], rec[1], rec[2:]
        else:
            return '???', '???', '???'

    def _command(self, *details):
        if not self.live:
            return False

        #self._send(*details)
        self.queuelock.acquire()
        if len(self.queue) and self.queue[-1][0] == details[0]:
            self.queue[-1] = details
        else:
            self.queue.append(details)
        self.queuelock.release()

        #msgid,status,cmd = self._get_resp()

        #if status != 'ACK':
        #    raise CommandFailed(cmd)
        #return True

    def simplecommand(self, description, *msgparts):
        try:
            self._command(*msgparts)
            print description
        except CommandFailed:
            print 'could not ', description


    def initstuff(self):

        self.simplecommand('get positions', 'lens,goto,read')
        self.simplecommand('calibrate zoom', 'lens,calibratezoom,write')
        self.simplecommand('center lens', 'lens,gotocenter,write')
        self.simplecommand('get positions', 'lens,goto,read')

########################################

    def img_color_temp(self, r, g, b):

        r = clamp(-0.5, 0.5, r)
        g = clamp(-0.5, 0.5, g)
        b = clamp(-0.5, 0.5, b)

        try:
            self._command('image,colortempgain,write',
                          'r', r, 'g', g, 'b', b)
        except CommandFailed:
            print 'failed to set brightness.'



    def img_brightness(self, r, g, b, o=0, a='off'):

        assert a in ('on', 'off')

        r = clamp(-0.5, 0.5, r)
        g = clamp(-0.5, 0.5, g)
        b = clamp(-0.5, 0.5, b)
        o = clamp(-0.5, 0.5, o)

        try:
            self._command('image,brightness,write',
                          'r', r, 'g', g, 'b', b, 'o', o, 'a', a)
        except CommandFailed:
            print 'failed to set brightness.'

    def img_contrast(self, r, g, b, o=0, a='off'):

        assert a in ('on', 'off')

        r = clamp(0.0, 2.0, r)
        b = clamp(0.0, 2.0, g)
        b = clamp(0.0, 2.0, b)
        o = clamp(0.0, 2.0, o)

        try:
            self._command('image,contrast,write',
                          'r', r, 'g', g, 'b', b, 'o', o, 'a', a)
        except CommandFailed:
            print 'failed to set contrast.'

    def projectionmode(self, mode='normal'):
        assert mode in ('curtain', 'testpattern', 'normal')

        self.simplecommand('set projection mode to ' + mode,
            'image,projectionmode,write', mode)

    def orient(self, direction='normal'):
        assert direction in ('normal', 'nsflip', 'ewflip', 'newsflip')

        self.simplecommand('Set orientation to ' + direction,
                           'image,orientation,write', direction)

    def mirror_park(self, mode='park'):
        assert mode in ('park', 'release')

        self.simplecommand(mode + ' mirror',
                           'image,mirrorpark,write', mode)



    def lens_stop_all(self):
        try:
            self._command('lens,stop,write')
            print 'stopped all lens motors'
        except CommandFailed:
            print 'could NOT stop all lens motors!!'

    def lens_move(self, axis, direction, time=300, speed=25):

        speed = clamp(0, 50, speed)
        time = clamp(300, 3000, time)

        try:
            self._command('lens,move,write', axis, direction, time, speed)
            print 'lens moved...'
        except CommandFailed:
            print 'could NOT move lens!'

    def lens_goto(self, h,v,f,z):
        self.simplecommand("Move Lens", 'lens,goto,write,h', h, 'v', v, 'f',f,'z',z)

    def lens_center(self):
        try:
            self._command('lens,gotocenter,write')
            print 'Centering Lens'
        except CommandFailed:
            print 'could not center lens!'

    def calibrate_zoom(self):
        try:
            self._command('lens,calibratezoom,write')
            print 'Calibrating Zoom'
        except CommandFailed:
            print 'could not calibrate zoom'

    def lamp_on(self, lamp=0):
        try:
            self._command('lpsu,on,write', lamp)
            print 'turned lamp %s on' % lamp
        except CommandFailed:
            print 'could not set lamp %s on' % lamp

    def lamp_off(self, lamp=0):
        try:
            self._command('lpsu,off,write', lamp)
            print 'turned lamp %s off' % lamp
        except CommandFailed:
            print 'could not set lamp %s off' % lamp

    def lamp_power(self, lamp=0, power=90):
        power = clamp(0, 100, power)

        try:
            self._command('lpsu,power,write', lamp, power)
            print 'set lamp %s power to %s' % (lamp, power)
        except CommandFailed:
            print 'could not set lamp %s power level to %s' % (lamp, power)



    def testpattern(self, pat='off', rate=60):
        assert pat in ('black', 'white', 'green', 'red', 'blue', 'checker',
                       'align', 'h_ramp', 'v_ramp', 'max_lumens', 'native_white',
                       'off', 'none')

        if pat == 'none':
            return self.projectionmode('normal')

        rate = clamp(25, 200, int(rate))

        self.simplecommand('test pattern: %s @ %s '% (pat, rate),
            'image,testpattern,write', pat, rate)

